import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class Landing extends Component {
  render() {
    return(
      <div style={{width: '100%', margin: 'auto'}}>
        <Grid className="landing-grid">
          <Cell col={12}>
            <img
        
              src="https://icon-library.net/images/avatar-icon/avatar-icon-27.jpg"
              alt="avatar"
              className="avatar-img"
              />

            <div className="banner-text">
              <h1>Full Stack Web Developer</h1>

            <hr/>

          <p> Java | Bootstrap | JavaScript | React | BI | Android </p>

        <div className="social-links">
         
          {/* LinkedIn */}
          <a href="https://www.linkedin.com/in/kamilwolski314" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-linkedin-square" aria-hidden="true" />
          </a>
   
     </div>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Landing;
