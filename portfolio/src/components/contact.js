import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent } from 'react-mdl';


class Contact extends Component {
  render() {
    return(
      <div className="contact-body">
        <Grid className="contact-grid">
          <Cell col={6}>
            <h2>Kamil Wolski</h2>
            <img
              src="https://scontent-amt2-1.xx.fbcdn.net/v/t1.0-9/p960x960/65080725_1322293951262992_4330371102262951936_o.jpg?_nc_cat=101&_nc_sid=110474&_nc_ohc=swxPBGlFoZoAX95H1CP&_nc_ht=scontent-amt2-1.xx&_nc_tp=6&oh=7ce8fbfbae4ea439a46cdf5455f39066&oe=5EB7F4E4"
              alt="avatar"
              style={{width: '75%', margin: 'auto'}}
               />
           

          </Cell>
          <Cell col={6}>
            <h2>Contact Me</h2>
            <hr/>

            <div className="contact-list">
              <List>
                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-phone-square" aria-hidden="true"/>
                    (+31) 642495089
                  </ListItemContent>
                </ListItem>

                

                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-envelope" aria-hidden="true"/>
                    info@kamilwolski.com
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-linkedin" aria-hidden="true"/>

                    LinkedIn
                  </ListItemContent>
                </ListItem>

          

              </List>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Contact;
