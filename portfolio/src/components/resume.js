import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skills from './skills';


class Resume extends Component {
  render() {
    return(
      <div>
        <Grid>
          <Cell col={4}>
            <div style={{textAlign: 'center'}}>
              <img
                src="https://scontent-ams4-1.xx.fbcdn.net/v/t1.0-9/92356051_1578259868999731_592058312585904128_n.jpg?_nc_cat=110&_nc_sid=110474&_nc_ohc=-VSyn-NmvDMAX-KnQep&_nc_ht=scontent-ams4-1.xx&oh=3fed09797bda618e624041e6fe21ca6d&oe=5EBA5EE2"
                alt="avatar"
                style={{height: '350px'}}
                 />
            </div>

            <h2 style={{paddingTop: '1em'}}>Kamil Wolski</h2>
            <h4 style={{color: 'grey'}}>Software Engineer / Web Developer</h4>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h5>Address</h5>
            <p>On request</p>
            <h5>Phone</h5>
            <p>(+31) 642495089</p>
            <h5>Email</h5>
            <p>info@kamilwolski.com</p>
            <h5>Web</h5>
            <p>kamilwolski.com</p>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          </Cell>
          <Cell className="resume-right-col" col={8}>
            <h2>Education</h2>


            <Education
              startYear={2002}
              endYear={2006}
              schoolName="My University"
              schoolDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
               />

               <Education
                 startYear={2007}
                 endYear={2009}
                 schoolName="My 2nd University"
                 schoolDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
                  />
                <hr style={{borderTop: '3px solid #e22947'}} />

              <h2>Experience</h2>

            <Experience
              startYear={2009}
              endYear={2012}
              jobName="First Job"
              jobDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
              />

              <Experience
                startYear={2012}
                endYear={2016}
                jobName="Second Job"
                jobDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
                />
              <hr style={{borderTop: '3px solid #e22947'}} />
              <h2>Skills</h2>
              <Skills
                skill="Java"
                progress={80}
                />
                <Skills
                  skill="Javascript"
                  progress={60}
                  />
                  <Skills
                  skill="Bootstrap"
                  progress={60}
                  />
                  <Skills
                    skill="React"
                    progress={70}
                    />
                    <Skills
                    skill="Business Intelligence" 
                    progress={60}
                    />
                    <Skills
                      skill="Android"
                      progress={80}
                      />


          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Resume;
